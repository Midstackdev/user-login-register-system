<?php 
	
	class Config {

		const DEVELOPMENT_URL = 'http://localhost/login';

		const PRODUCTION_URL = '';

		const SMTP_HOST = 'smtp.mailtrap.io';

		const SMTP_USER = '';

		const SMTP_PASSWORD = '';

		const SMTP_PORT = 2525;

	}