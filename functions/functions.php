<?php 

	// Import PHPMailer classes into the global namespace
	// These must be at the top of your script, not inside a function
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	require "vendor/autoload.php";


	
	function clean($string){

		return htmlentities($string);
	}

	function redirect($location){

		return header("Location: {$location}");	
	}


	function set_message($message){

		if(!empty($message)){

			$_SESSION['message'] = $message;
		}else {

			$message = "";
		}
	}


	function display_message(){

		if(isset($_SESSION['message'])){

			echo $_SESSION['message'];

			unset($_SESSION['message']);
		}
	}


	function token_generator(){

		$token = $_SESSION['token'] = md5(uniqid(mt_rand(), true));

		return $token;
	}

	function validation_errors($error_message){

		$error_message = <<<DELIMITER

		<div class="alert alert-danger alert-dismissible" role="alert">
		  <strong> $error_message !</strong>
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    <span aria-hidden="true">&times;</span>
		  </button>
		</div>
DELIMITER;

		return $error_message;
	}

	function email_exists($email){

		$sql = "SELECT id FROM users WHERE email = '$email' ";

		$result = query($sql);

		if(row_count($result) == 1){

			return true;
		} else {

			return false;
		}
	}

	function username_exists($username){

		$sql = "SELECT id FROM users WHERE username = '$username' ";

		$result = query($sql);

		if(row_count($result) == 1){

			return true;
		} else {
			
			return false;
		} 
	}


	function send_email($email=null, $subject=null, $msg=null, $headers=null){

		$mail = new PHPMailer(true); 
		try {                          // Passing `true` enables exceptions
		    //Server settings
		    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
		    $mail->isSMTP();                                      // Set mailer to use SMTP
		    $mail->Host = Config::SMTP_HOST;  // Specify main and backup SMTP servers
		    $mail->SMTPAuth = true;                               // Enable SMTP authentication
		    $mail->Username = Config::SMTP_USER;                 // SMTP username
		    $mail->Password = Config::SMTP_PASSWORD;                           // SMTP password
		    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		    $mail->Port = Config::SMTP_PORT;                                    // TCP port to connect to

		    //Recipients
		    $mail->setFrom('info@ocsghana.com', 'OCS Ghana');
		    //$mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
		    $mail->addAddress($email);               // Name is optional
		    // $mail->addReplyTo('info@example.com', 'Information');
		    // $mail->addCC('cc@example.com');
		    // $mail->addBCC('bcc@example.com');

		    //Content
		    $mail->isHTML(true);
		    $mail->CharSet = 'UTF-8';                                  // Set email format to HTML
		    $mail->Subject = $subject;
		    $mail->Body    = $msg;
		    $mail->AltBody = $msg;

			$mail->send();
			echo 'Message has been sent';
		} catch (Exception $e) {
		    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
		}

		//return mail($email, $subject, $msg, $headers);

			//ini_set(varname, newvalue)
	}




/*************** register form validate function*********************/


	function validate_user_registration(){

		$errors = [];

		$min = 3;
		$max = 20;

		if($_SERVER['REQUEST_METHOD'] == "POST"){

			$first_name 		= escape(clean($_POST['first_name'])); 
			$last_name 			= escape(clean($_POST['last_name'])); 
			$username 			= escape(clean($_POST['username'])); 
			$email 				= escape(clean($_POST['email'])); 
			$password 			= escape(clean($_POST['password']));
			$confirm_password 	= escape(clean($_POST['confirm_password']));  

			if(strlen($first_name) < $min){

				$errors[] = "Your first name cannot be less than {$min} characters";
			}

			if(strlen($first_name) > $max){

				$errors[] = "Your first name cannot be more than {$max} characters";
			}

			if(strlen($last_name) < $min){

				$errors[] = "Your last name cannot be less than {$min} characters";
			}

			if(strlen($last_name) > $max){

				$errors[] = "Your last name cannot be more than {$max} characters";
			}

			if(strlen($username) < $min){

				$errors[] = "Your username cannot be less than {$min} characters";
			}

			if(strlen($username) > $max){

				$errors[] = "Your username cannot be more than {$max} characters";
			}

			if($password !== $confirm_password){

				$errors[] = "Your passwords do not match";
			}

			if(email_exists($email)){

				$errors[] = "Sorry your email is already registered";
			}

			if(username_exists($username)){

				$errors[] = "Sorry your username is already taken";
			}

			if(!empty($errors)){

				foreach ($errors as $error) {

					echo validation_errors($error);
				}
			} else {

				if(register_user($first_name, $last_name, $email, $username, $password)){

					set_message("<p class='bg-success text-center'>Please check your email for confirmation</p>");

					redirect("index.php");
				}
			}
		}


	}


	/*************** register user function*********************/

	function register_user($first_name, $last_name, $email, $username, $password){


		if(email_exists($email)){

			return false;
		} else if(username_exists($username)) {

			return false;
		} else {

			$password = password_hash($password, PASSWORD_BCRYPT, array('cost'=>12));

			$validation = md5((int)$username . (int)microtime());

			$sql = "INSERT INTO users(first_name, last_name, username, email, password, validation_code, active)";
			$sql .= " VALUES('$first_name', '$last_name', '$username', '$email', '$password', '$validation', 0)";
			$result = query($sql);
			confirm($result);

			$subject = "Activate Account";
			$msg = " Please click the link below to activate your Account
			<a href = \" ". Config::DEVELOPMENT_URL ."/activate.php?email=$email&code=$validation\">
			LINK HERE</a>";

			$headers = "From: noreply@yourwebsite.com";

			send_email($email, $subject, $msg, $headers);

			return true;
		}

	}


/*************** activate user function*********************/

	function activate_user(){

		if($_SERVER['REQUEST_METHOD'] == 'GET'){

			if(isset($_GET['email'])){

				$email = clean($_GET['email']);

				$validation_code = clean($_GET['code']);

				$sql = "SELECT id FROM users WHERE email = '".escape($_GET['email'])."' 
					AND validation_code = '".escape($_GET['code'])."' "	;
				$result = query($sql);
				confirm($result);

				if(row_count($result) == 1){

					$sql = "UPDATE users SET active = 1, validation_code = 0 WHERE email = '".escape($_GET['email'])."' 
						AND validation_code = '".escape($_GET['code'])."' "	;
					$result = query($sql);
					confirm($result);

					set_message("<p class='bg-success text-center'>Your account has been activated please login</p>");
					redirect("login.php");
				} else{
					set_message("<p class='bg-danger text-center'>Sorry Your account could not be activated please retry</p>");
					redirect("login.php");

				}

				

			}

			
		}
	}



	/***************validate user login function*********************/

	function validate_user_login(){

		$errors = [];

		$min = 3;
		$max = 20;

		if($_SERVER['REQUEST_METHOD'] == "POST"){

			$email 				= escape(clean($_POST['email'])); 
			$password 			= escape(clean($_POST['password']));
			$remember			= isset($_POST['remember']);


			if(empty($email)){

				$errors[] = "Email required";
			}

			if(empty($password)){

				$errors[] = "Your passwords is required";
			}



			if(!empty($errors)){

				foreach ($errors as $error) {

					echo validation_errors($error);
				}
			} else{

				if(login_user($email, $password, $remember)){

					redirect("admin.php");
				}else{

					echo validation_errors("Your credentials are not correct");
				}

			}



		}


	}


	/*************** user login function*********************/


	function login_user($email, $password, $remember){

		$sql = "SELECT password, id FROM users WHERE email = '$email' AND active = 1";
		$result = query($sql);
		confirm($result);

		if(row_count($result) == 1){

			$row = fetch($result);

			$db_password = $row['password'];

			if(password_verify($password, $db_password)){

				if($remember == "on"){

					setcookie('email', $email, time() + 86400);
				}

				$_SESSION['email'] = $email;

				return true;
			} else{

				return false;
			}
		

			return true;
		}else{

			return false;
		}


	}


/*************** logged in function*********************/

	function logged_in(){

		if(isset($_SESSION['email']) || isset($_COOKIE['email'])){

			return true;
		}else {

			return false;
		}
	}


/*************** Recover password function*********************/	

	function recover_password(){

		if($_SERVER['REQUEST_METHOD'] == "POST"){

			if(isset($_SESSION['token']) && $_SESSION['token'] == $_POST['token']){

					$email = escape(clean($_POST['email']));

					if(email_exists($email)){

						$validation_code = md5((int)$email . (int)microtime());

						$sql = "UPDATE users SET validation_code = '".escape($validation_code)."' WHERE email = '$email' ";
						$result = query($sql);
						confirm($result);

						setcookie("temp_access_code", $validation_code, time()+180);

						$subject = "PLease reset your password";
						$message = " Here is your password reset code {$validation_code}.

							Click here to reset your password <a href = \"http://localhost/login/code.php?email={$email}&code={$validation_code}\">LINK HERE</a>";

						$headers = "From: noreply@yourwebsite.com";	

						if(!send_email($email, $subject, $message, $headers))

							echo validation_errors("Email could not be sent");

						set_message("<p class='bg-success text-center'>Please check your email or spam for pasword reset code.</p>");
						redirect("index.php");

					} else{

						echo validation_errors("This email does not exist");
					}

			} else{

				redirect("index.php");
			}

			if(isset($_POST['cancel_submit']))
				redirect("login.php");

			
		} //post request


	}//function



/*************** Validate code function*********************/
	
	function validate_code(){

		if(isset($_COOKIE["temp_access_code"])){

			if(!isset($_GET['email']) && !isset($_GET['code'])){

				redirect("index.php");

			} else if(empty($_GET['email']) || empty($_GET['code'])){

				redirect("index.php");

			} else{

				if(isset($_POST['code'])){

					$validation_code = clean($_POST[code]);
					$email = clean($_GET['email']);

					$sql = "SELECT id FROM users WHERE validation_code = '".escape($validation_code)."' 
						AND email = '".escape($email)."' ";
					$result = query($sql);
					confirm($result);

					if(row_count($result) == 1){

						setcookie("temp_access_code", $validation_code, time()+900);

						redirect("reset.php?email=$email&code=$validation_code");
					} else{

						echo validation_errors("Sorry wrong validation code");
					}
				}
			}
			

		}else {

			set_message("<p class='bg-danger text-center'>Sorry your session Expired.</p>");
			redirect("recover.php");
		}
	}

/*************** Password reset function*********************/

	function password_reset(){

		if(isset($_COOKIE["temp_access_code"])){

			if(isset($_GET['email']) && isset($_GET['code'])){

				if((isset($_SESSION['token']) && isset($_POST['token'])) && $_SESSION['token'] === $_POST['token']){

					if($_POST['password'] === $_POST['confirm_password']){

						$password = escape(clean($_POST['password']));
						$password = password_hash($password, PASSWORD_BCRYPT, array('cost'=>12));;

						$sql = "UPDATE users SET password = '$password', validation_code = 0, active = 1 WHERE email = '".escape($_GET['email'])."' 
							AND validation_code =  '".escape($_GET['code'])."' ";
						$result = query($sql);
						confirm($result);	

						set_message("<p class='bg-success text-center'>Your password has been updated, Please login.</p>");
						redirect("login.php");

					} else{
						echo validation_errors("Sorry passwords do not match");
					}
				

				}
			}

		}else {

			set_message("<p class='bg-danger text-center'>Sorry your session is timed out.</p>");
			redirect("recover.php");
		}
	}












?>