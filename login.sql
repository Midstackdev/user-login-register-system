-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2019 at 09:09 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `validation_code` text NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `email`, `password`, `validation_code`, `active`) VALUES
(1, 'Admino', 'Bosso', 'admin', 'admin@admin.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'no', 1),
(2, 'Alfredo', 'Delavega', 'aspensa', 'lfrdsmit@ymail.com', '36f17c3939ac3e7b2fc9396fa8e953ea', 'b4b147bc522828731f1a016bfa72c073', 1),
(3, 'Sume', 'memner', 'asader', 'ewahoho2002@yahoo.co.uk', '53066aee5deb25f2805c369c101abd81', '0', 1),
(4, 'Samerdem', 'Stouch', 'amersin', 'lfrdsmit@oloo.com', 'b427ebd39c845eb5417b7f7aaf1f9724', 'cfcd208495d565ef66e7dff9f98764da', 0),
(5, 'Vero', 'Awusi', 'awus', 'ewahoho@yahoo.co.uk', 'e10adc3949ba59abbe56e057f20f883e', 'cfcd208495d565ef66e7dff9f98764da', 0),
(6, 'joe', 'doe', 'joe', 'ewahoho2@yahoo.co.uk', 'e10adc3949ba59abbe56e057f20f883e', 'cfcd208495d565ef66e7dff9f98764da', 0),
(7, 'Fedo', 'smith', 'sememr', 'lfrdsmit@oloco.com', 'e10adc3949ba59abbe56e057f20f883e', 'cfcd208495d565ef66e7dff9f98764da', 0),
(8, 'Alfredo', 'Masser', 'oloda', 'dele@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'cfcd208495d565ef66e7dff9f98764da', 0),
(9, 'Sumbre', 'Undo', 'sadio', 'sadio@yahoo.co.uk', '5f4dcc3b5aa765d61d8327deb882cf99', 'cfcd208495d565ef66e7dff9f98764da', 0),
(10, 'Jin', 'Yang', 'jiang', 'yang@yahoo.com', '$2y$12$dfas19OI6TcLrRsTF6to.uIhgk44jwRtilhU1gDdxulRluqyeIGWS', '0', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
